﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Net
{
    public class TcpClient : IDisposable
    {
        public enum State { DISCONNECTED, CONNECTING, CONNECTED }

        private readonly SocketAsyncEventArgs receiveEventArgs;
        private readonly SocketAsyncEventArgs sendEventArgs;

        private Socket client;
        private bool disposed;

        public State ConnectionState { get; private set; }

        public TcpClient()
        {
            receiveEventArgs = new SocketAsyncEventArgs();
            receiveEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(ReceiveEventArgCompleted);
            receiveEventArgs.UserToken = client;
            receiveEventArgs.SetBuffer(new Memory<byte>(new byte[1024]));

            sendEventArgs = new SocketAsyncEventArgs();
            sendEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(SendEventArgCompleted);
            sendEventArgs.UserToken = client;
        }

        #region Connection
        public void Connect(string remoteAddress, int remotePort)
        {
            Connect(new IPEndPoint(IPAddress.Parse(remoteAddress), remotePort));
        }

        public void Connect(IPEndPoint remoteEndPoint)
        {
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            KeepAlive(client, 1, 1000, 1000);

            receiveEventArgs.RemoteEndPoint = remoteEndPoint;
            sendEventArgs.RemoteEndPoint = remoteEndPoint;

            SocketAsyncEventArgs connectEventArg = new SocketAsyncEventArgs
            {
                UserToken = client,
                RemoteEndPoint = remoteEndPoint
            };

            connectEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(ConnectEventArgCompleted);

            ConnectionState = State.CONNECTING;
            bool willRaiseEvent = client.ConnectAsync(connectEventArg);
            if (!willRaiseEvent)
            {
                ProcessConnect(connectEventArg);
            }
        }

        private void KeepAlive(Socket socket, int onOff, int keepAliveTime, int keepAliveInterval)
        {
            byte[] buffer = new byte[12];
            BitConverter.GetBytes(onOff).CopyTo(buffer, 0);
            BitConverter.GetBytes(keepAliveTime).CopyTo(buffer, 4);
            BitConverter.GetBytes(keepAliveInterval).CopyTo(buffer, 8);

            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, buffer);
        }

        private void ConnectEventArgCompleted(object sender, SocketAsyncEventArgs e)
        {
            ProcessConnect(e);
        }

        private void ProcessConnect(SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                ConnectionState = State.CONNECTED;
                HandleConnectionEvent();

                StartReceive();
            }
            else
            {
                ConnectionState = State.DISCONNECTED;
                HandleErrorEvent(e.SocketError);
            }
        }
        #endregion

        protected virtual void HandleConnectionEvent()
        {

        }

        protected virtual void HandleDisconnectionEvent()
        {

        }

        protected virtual void HandleReceivedData(Memory<byte> buffer)
        {

        }

        protected virtual void HandleErrorEvent(SocketError socketError)
        {

        }

        #region Send Message
        public void Send(byte[] data)
        {
            if (ConnectionState != State.CONNECTED)
            {
                return;
            }

            // Send message
            sendEventArgs.SetBuffer(data, 0, data.Length);
            bool willRaiseEvent = client.SendAsync(sendEventArgs);
            if (!willRaiseEvent)
            {
                ProcessSend(sendEventArgs);
            }
        }

        private void SendEventArgCompleted(object sender, SocketAsyncEventArgs e)
        {
            ProcessSend(e);
        }

        private void ProcessSend(SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success)
            {
                if (ConnectionState != State.DISCONNECTED)
                {
                    HandleErrorEvent(e.SocketError);
                    Close();
                }
            }
        }
        #endregion

        #region Receive Message
        private void StartReceive()
        {
            // Read the next block of data send from the client
            bool willRaiseEvent = client.ReceiveAsync(receiveEventArgs);
            if (!willRaiseEvent)
            {
                ProcessReceive(receiveEventArgs);
            }
        }

        private void ReceiveEventArgCompleted(object sender, SocketAsyncEventArgs e)
        {
            ProcessReceive(e);
        }

        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            // Check if the remote host closed the connection
            if (e.BytesTransferred == 0 || e.SocketError != SocketError.Success)
            {
                if (ConnectionState != State.DISCONNECTED)
                {
                    if (e.SocketError != SocketError.Success)
                    {
                        HandleErrorEvent(e.SocketError);
                    }
                    Close();
                }
                return;
            }

            HandleReceivedData(e.MemoryBuffer.Slice(0, e.BytesTransferred));

            // Read the next block of data send from the client
            bool willRaiseEvent = client.ReceiveAsync(e);
            if (!willRaiseEvent)
            {
                ProcessReceive(e);
            }
        }
        #endregion

        public void Close()
        {
            if (ConnectionState == State.DISCONNECTED)
            {
                return;
            }

            try
            {
                client.Shutdown(SocketShutdown.Both);
            }
            catch (Exception)  // Throws if client process has already closed
            {

            }
            finally
            {
                client.Close();
            }

            ConnectionState = State.DISCONNECTED;
            HandleDisconnectionEvent();
        }

        #region Dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Close();
                }
                disposed = true;
            }
        }
        #endregion
    }
}
