﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Net
{
    public class ConnectionFailedEventArgs : EventArgs
    {
        public SocketError SocketError { get; set; }

        public ConnectionFailedEventArgs(SocketError socketError)
        {
            SocketError = socketError;
        }
    }

    public class TcpServer : IDisposable
    {
        private readonly List<TcpSession> sessions = new List<TcpSession>(128);

        private Socket listenSocket;
        private bool disposed;

        public bool IsRunning { get; private set; }

        internal IReadOnlyList<TcpSession> Sessions
        {
            get
            {
                return sessions;
            }
        }

        public TcpServer()
        {

        }

        public void Start(int listenPort)
        {
            Start(new IPEndPoint(IPAddress.Parse("127.0.0.1"), listenPort));
        }

        public void Start(string localAddress, int listenPort)
        {
            Start(new IPEndPoint(IPAddress.Parse(localAddress), listenPort));
        }

        /// <summary>
        /// Starts the server such that it is listening for incoming connection requests
        /// </summary>
        /// <param name="localEndPoint">The endpoint which the server will listening for connection requests on</param>
        public void Start(IPEndPoint localEndPoint)
        {
            if (!IsRunning)
            {
                IsRunning = true;

                // Create the socket which listens for incoming connections
                listenSocket = new Socket(localEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                KeepAlive(listenSocket, 1, 1000, 1000);

                if (localEndPoint.AddressFamily == AddressFamily.InterNetworkV6)
                {
                    // Set to Dual Mode (Support IPv4 & IPv6)   
                    listenSocket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.IPv6Only, false);
                    listenSocket.Bind(new IPEndPoint(IPAddress.IPv6Any, localEndPoint.Port));
                }
                else
                {
                    listenSocket.Bind(localEndPoint);
                }

                // Start the server with a listen backlog of 100 connections
                listenSocket.Listen(100);

                // Post accepts on the listening socket
                StartAccept(null);
            }
        }

        private void KeepAlive(Socket socket, int onOff, int keepAliveTime, int keepAliveInterval)
        {
            byte[] buffer = new byte[12];
            BitConverter.GetBytes(onOff).CopyTo(buffer, 0);
            BitConverter.GetBytes(keepAliveTime).CopyTo(buffer, 4);
            BitConverter.GetBytes(keepAliveInterval).CopyTo(buffer, 8);

            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, buffer);
        }

        public void Stop()
        {
            if (IsRunning)
            {
                IsRunning = false;
                listenSocket.Close();
                for (int i = 0; i < sessions.Count; i++)
                {
                    sessions[i].Close();
                }
            }
        }

        #region Accept
        /// <summary>
        /// Begins an operation to accept a connection request from the client
        /// </summary>
        /// <param name="acceptEventArg">The context object to use when issuing the accept operation on the server's listening socket</param>
        private void StartAccept(SocketAsyncEventArgs acceptEventArg)
        {
            if (acceptEventArg == null)
            {
                acceptEventArg = new SocketAsyncEventArgs();
                acceptEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(AcceptEventArgCompleted);
            }
            else
            {
                // Socket must be cleared since the context object is being reused
                acceptEventArg.AcceptSocket = null;
            }

            bool willRaiseEvent = listenSocket.AcceptAsync(acceptEventArg);
            if (!willRaiseEvent)
            {
                ProcessAccept(acceptEventArg);
            }
        }

        /// <summary>
        /// This method is the callback method associated with Socket.AcceptAsync operations and is invoked when an accept operation is complete
        /// </summary>
        private void AcceptEventArgCompleted(object sender, SocketAsyncEventArgs e)
        {
            ProcessAccept(e);
        }

        private void ProcessAccept(SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success)
            {
                // TODO: Handle the error
                return;
            }

            // Get the socket for the accepted client connection and put it into the ReadEventArg object user token
            Socket socket = e.AcceptSocket;
            KeepAlive(socket, 1, 1000, 1000);

            TcpSession session = CreateSession(socket);
            sessions.Add(session);
            session.Start();

            // Accept the next connection request
            StartAccept(e);
        }
        #endregion

        protected virtual TcpSession CreateSession(Socket socket)
        {
            return new TcpSession(this, socket);
        }

        public void Broadcast(byte[] data)
        {
            for (int i = 0; i < sessions.Count; i++)
            {
                if (sessions[i].IsRunning)
                {
                    sessions[i].Send(data);
                }
            }
        }

        #region Dispose  
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Stop();
                }
                disposed = true;
            }
        }
        #endregion
    }
}
