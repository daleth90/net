﻿using System;
using System.Net.Sockets;

namespace Net
{
    public class TcpSession
    {
        private readonly TcpServer server;
        private readonly AsyncUserToken token;
        private readonly SocketAsyncEventArgs receiveEventArgs;
        private readonly SocketAsyncEventArgs sendEventArgs;

        private bool disposed;

        public bool IsRunning { get; private set; }

        protected TcpServer Server { get { return server; } }

        public TcpSession(TcpServer server, Socket socket)
        {
            this.server = server;
            token = new AsyncUserToken
            {
                Socket = socket,
            };

            receiveEventArgs = new SocketAsyncEventArgs();
            receiveEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(IOCompleted);
            receiveEventArgs.UserToken = token;
            receiveEventArgs.SetBuffer(new Memory<byte>(new byte[1024]));

            sendEventArgs = new SocketAsyncEventArgs();
            sendEventArgs.Completed += new EventHandler<SocketAsyncEventArgs>(IOCompleted);
            sendEventArgs.UserToken = token;
        }

        public void Start()
        {
            IsRunning = true;
            HandleConnectionEvent();

            // As soon as the client is connected, post a receive to the connection
            bool willRaiseEvent = token.Socket.ReceiveAsync(receiveEventArgs);
            if (!willRaiseEvent)
            {
                ProcessReceive(receiveEventArgs);
            }
        }

        protected virtual void HandleConnectionEvent()
        {

        }

        protected virtual void HandleDisconnectionEvent()
        {

        }

        protected virtual void HandleReceivedData(Memory<byte> buffer)
        {

        }

        protected virtual void HandleError(SocketError socketError)
        {

        }

        /// <summary>
        /// This method is called whenever a receive or send operation is completed on a socket
        /// </summary>
        /// <param name="e">SocketAsyncEventArg associated with the completed receive operation</param>
        private void IOCompleted(object sender, SocketAsyncEventArgs e)
        {
            // Determine which type of operation just completed and call the associated handler
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Receive:
                    ProcessReceive(e);
                    break;
                case SocketAsyncOperation.Send:
                    ProcessSend(e);
                    break;
                default:
                    throw new ArgumentException("The last operation completed on the socket was not a receive or send");
            }
        }

        #region Receive Message
        /// <summary>
        /// This method is invoked when an asynchronous receive operation completes.
        /// If the remote host closed the connection, then the socket is closed.
        /// </summary>
        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            // Check if the remote host closed the connection
            if (e.BytesTransferred == 0 || e.SocketError != SocketError.Success)
            {
                Close();
                return;
            }

            HandleReceivedData(e.MemoryBuffer.Slice(0, e.BytesTransferred));

            // Read the next block of data send from the client
            AsyncUserToken token = e.UserToken as AsyncUserToken;
            bool willRaiseEvent = token.Socket.ReceiveAsync(e);
            if (!willRaiseEvent)
            {
                ProcessReceive(e);
            }
        }
        #endregion

        #region Send Message
        public void Send(byte[] data)
        {
            sendEventArgs.SetBuffer(data, 0, data.Length);
            bool willRaiseEvent = token.Socket.SendAsync(sendEventArgs);
            if (!willRaiseEvent)
            {
                ProcessSend(sendEventArgs);
            }
        }

        /// <summary>
        /// This method is invoked when an asynchronous send operation completes
        /// </summary>
        private void ProcessSend(SocketAsyncEventArgs e)
        {
            // Check if the remote host closed the connection
            if (e.SocketError != SocketError.Success)
            {
                Close();
                return;
            }
        }
        #endregion

        public void Close()
        {
            IsRunning = false;

            // Close the socket associated with the client
            Socket socket = token.Socket;
            try
            {
                socket.Shutdown(SocketShutdown.Both);
            }
            catch (Exception)  // Throws if client process has already closed
            {

            }
            finally
            {
                socket.Close();
            }

            receiveEventArgs.Completed -= new EventHandler<SocketAsyncEventArgs>(IOCompleted);
            receiveEventArgs.Dispose();

            sendEventArgs.Completed -= new EventHandler<SocketAsyncEventArgs>(IOCompleted);
            sendEventArgs.Dispose();

            HandleDisconnectionEvent();
        }

        #region Dispose  
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Close();
                }
                disposed = true;
            }
        }
        #endregion
    }
}
