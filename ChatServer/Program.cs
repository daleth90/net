﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Net.Example
{
    public class ChatSession : TcpSession
    {
        public ChatSession(ChatServer server, Socket socket) : base(server, socket)
        {

        }

        protected override void HandleConnectionEvent()
        {
            Console.WriteLine("接收連線");
        }

        protected override void HandleDisconnectionEvent()
        {
            Console.WriteLine("連線中斷");
        }

        protected override void HandleReceivedData(Memory<byte> buffer)
        {
            Console.WriteLine(Encoding.ASCII.GetString(buffer.Span));
            Server.Broadcast(buffer.ToArray());
        }
    }

    public class ChatServer : TcpServer
    {
        protected override TcpSession CreateSession(Socket socket)
        {
            return new ChatSession(this, socket);
        }
    }

    class Program
    {
        static void Main()
        {
            ChatServer server = new ChatServer();
            server.Start(8080);

            while (Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                continue;
            }

            server.Stop();
            Console.WriteLine("Server Stop");
        }
    }
}
