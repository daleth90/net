﻿using System;
using System.Text;

namespace Net.Example
{
    public class ChatClient : TcpClient
    {
        protected override void HandleConnectionEvent()
        {
            Console.WriteLine("\nConnection Success!!!");
            Console.WriteLine("You can try message echo now. Press Esc to disconnect");
        }

        protected override void HandleDisconnectionEvent()
        {
            Console.WriteLine("\nDisconnect! Press Num 0 to reconnect");
        }

        protected override void HandleReceivedData(Memory<byte> buffer)
        {
            Console.WriteLine("[Echo] " + Encoding.ASCII.GetString(buffer.Span));
        }

        protected override void HandleErrorEvent(System.Net.Sockets.SocketError socketError)
        {
            Console.WriteLine("[ERROR] " + socketError);
        }
    }

    class Program
    {
        private static void Main()
        {
            ChatClient client = new ChatClient();

            Console.WriteLine("Welcome to Client Example! Press Num 0 to connect");
            while (true)
            {
                if (client.ConnectionState == TcpClient.State.DISCONNECTED)
                {
                    ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                    if (keyInfo.KeyChar == '0')
                    {
                        Console.WriteLine("Connecting...");
                        client.Connect("127.0.0.1", 8080);
                    }
                }
                else if (client.ConnectionState == TcpClient.State.CONNECTED)
                {
                    if (TryReadLine(out string message))
                    {
                        client.Send(Encoding.ASCII.GetBytes(message));
                    }
                    else
                    {
                        client.Close();
                    }
                }
            }
        }

        private static bool TryReadLine(out string result)
        {
            StringBuilder sb = new StringBuilder();
            while (true)
            {
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.Escape)
                {
                    result = "";
                    return false;
                }
                else if (keyInfo.Key == ConsoleKey.Enter)
                {
                    Console.Write("\n");
                    result = sb.ToString();
                    return true;
                }
                else if (keyInfo.Key == ConsoleKey.Backspace && sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                    Console.Write("\b \b");
                }
                else if (keyInfo.KeyChar != 0)
                {
                    sb.Append(keyInfo.KeyChar);
                    Console.Write(keyInfo.KeyChar);
                }
            }
        }
    }
}
